<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Llista de productes') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead>
                        <tr>
                            <th class="px-6 py-3 bg-gray-50 text-center text-xs text-gray-500 uppercase tracking-wider font-semibold">
                                ID
                            </th>
                            <th class="px-6 py-3 bg-gray-50 text-center text-xs text-gray-500 uppercase tracking-wider font-semibold">
                                Name
                            </th>
                            <th class="px-6 py-3 bg-gray-50 text-center text-xs text-gray-500 uppercase tracking-wider font-semibold">
                                Price
                            </th>
                            <th class="px-6 py-3 bg-gray-50 text-center text-xs text-gray-500 uppercase tracking-wider font-semibold">
                                Description
                            </th>
                            <th class="px-6 py-3 bg-gray-50 text-center text-xs text-gray-500 uppercase tracking-wider font-semibold">
                                Actions
                            </th>
                        </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">
                        @foreach ($products as $product)
                            <tr>
                                <td class="px-6 py-4 whitespace-nowrap text-lg ">
                                    {{ $product->id }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-lg ">
                                    {{ $product->name }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-600">
                                    {{ $product->price }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-600">
                                    {{ $product->descrip }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap ">
                                    <a href="{{ route('product.delete', $product->id) }}" class="bg-red-600 text-white py-2 px-4 rounded
                                    hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-red-500 focus:ring-opacity-50">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
