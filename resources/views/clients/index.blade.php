<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Clients') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1 class="text-2xl font-bold mb-6">Client List</h1>
                    <ul>
                        @foreach ($clients as $client)
                            <li>
                                <strong>{{ $client->name }}</strong><br>
                                Email: {{ $client->email }}<br>
                                Phone: {{ $client->phone }}<br>
                                Address: {{ $client->address }}<br>
                                <a href="{{ route('client.delete', $client->id) }}">Delete</a>
                            </li>
                        @endforeach
                    </ul>
                    <a href="{{ route('clients.create') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Add Client</a>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
