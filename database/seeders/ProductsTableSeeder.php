<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('products')->insert([
            ['name' => 'Marcelo', 'descrip' => 'agachate y conocelo', 'quantity' => 3, 'price' => 33.33],
            ['name' => 'Ignacio', 'descrip' => 'el que esta en el gimnasio', 'quantity' => 1, 'price' => 33.33],
            ['name' => 'Pedro', 'descrip' => 'pedro pedro peeedro pe', 'quantity' => 1, 'price' => 33.33],
            ['name' => 'Rosa', 'descrip' => 'Pastel', 'quantity' => 1, 'price' => 33.33],
            ['name' => 'Mariano', 'descrip' => 'Rajoy', 'quantity' => 1, 'price' => 33.33],
            ['name' => 'Susana', 'descrip' => 'Horia', 'quantity' => 1, 'price' => 33.33],
            ['name' => 'Juan Palomo', 'descrip' => 'yo me lo guiso yo me lo como', 'quantity' => 1, 'price' => 33.33]
        ]);
    }
}
