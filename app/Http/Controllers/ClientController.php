<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class ClientController extends Controller
{
    public function all(){
        $clients = Client::all();
        return view('allclients')->with('clients', $clients);
    }

    public function delete(Client $client):RedirectResponse{
        $client->delete();
        return Redirect::route('clients.all');
    }

    public function deleteconfirmation(Client $client){
        return view('deleteclientsconfirmation')->with('client', $client);
    }

    public function insert(Request $request):RedirectResponse{
        $validated = $request->validate([
            'name' => 'required|unique:clients|max:100',
            'surname' => 'max:100',
            'email' => 'required|unique:clients|max:100',
            'phonenumber' => 'required|unique:clients|max:10',
        ]);

        DB::table('clients')->insert(['name'=>$validated['name'], 'surname'=>$validated['surname'], 'email'=>$validated['email'], 'phonenumber'=>$validated['phonenumber'], 'postalcode'=>$validated['postalcode'], 'address'=>$validated['address']]);

        return Redirect::route('clients.all');
    }
}
