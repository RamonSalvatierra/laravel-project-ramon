<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\DiscountController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProfileController;
use App\Models\Category;
use App\Models\Discount;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::get('/products', [ProductController::class,'all'])->name('products.all');

Route::get('product/{product}/delete', [ProductController::class,'delete'])->name('products.delete');

Route::get('product/{product}/deleteconfirmation', [ProductController::class,'deleteconfirmation'])->name('products.deleteconfirmation');

Route::get('/products/form', function () {
    $categories = Category::all();
    $discounts = Discount::all();
    return view('productform', compact('categories', 'discounts'));
})->name("products.form");

Route::post('products/insert', [ProductController::class,'insert'])->name('products.insert');

Route::get('/clients', [ClientController::class,'all'])->name('clients.all');

Route::get('client/{client}/delete', [ClientController::class,'delete'])->name('clients.delete');

Route::get('client/{client}/deleteconfirmation', [ClientController::class,'deleteconfirmation'])->name('clients.deleteconfirmation');

Route::get('/clients/form', function () {
    return view('clientsform');
})->name("clients.form");

Route::post('clients/insert', [ClientController::class,'insert'])->name('clients.insert');


Route::get('/orders', [OrderController::class, 'all'])->name('orders.all');

Route::get('/orders/create', [OrderController::class, 'create'])->name('orders.create');
Route::post('/orders', [OrderController::class, 'store'])->name('orders.store');

Route::get('/categories', [CategoryController::class,'all'])->name('categories.all');

Route::get('categories/{category}/delete', [CategoryController::class,'delete'])->name('categories.delete');

Route::get('categories/{category}/deleteconfirmation', [CategoryController::class,'deleteconfirmation'])->name('category.deleteconfirmation');

Route::get('/categories/form', function () {
    return view('categoryform');
})->name("categories.form");

Route::post('category/insert', [CategoryController::class,'insert'])->name('category.insert');
Route::get('/categories/edit/{category}', [CategoryController::class, 'editcategory'])->name('categories.editcategory');
Route::post('/categories/update/{category}', [CategoryController::class, 'updatecategory'])->name('categories.updatecategory');


require __DIR__.'/auth.php';
